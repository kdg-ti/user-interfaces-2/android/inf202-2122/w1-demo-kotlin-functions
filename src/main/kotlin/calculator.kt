fun main (){
   println("sum function 3 + 2 = ${sumFun(3.0,2.0)}")
   println("diff compact function 3 - 2 = ${diffCompact(3.0,2.0)}")
   println("product lambda variabele 3 * 2 = ${productLambda(3.0,2.0)}")
   println("quotient calculator  3 / 2 = " + calculator(3.0,2.0,{a,b->a/b}))
    println("quotient calculator  trailing lambda 3 % 2 = " + calculator(3.0,2.0) { a, b -> a % b })
println("converter 20° = in Fahrenheit " + converter(20.0) {it*9/5+32})

}

fun sumFun(a:Double, b:Double):Double{
    return a+b
}

fun diffCompact (a:Double, b:Double) = a-b

val productLambda :(Double,Double)-> Double = {a,b->a*b}

fun calculator (a:Double, b:Double,operation:(Double,Double)-> Double):Double{
    return operation(a,b)
}

// compacte vorm calculator function
//fun calculator (a:Double, b:Double,operation:(Double,Double)-> Double)=
//     operation(a,b)
fun converter (a:Double,operation:(Double)-> Double):Double{
    return operation(a)
}

