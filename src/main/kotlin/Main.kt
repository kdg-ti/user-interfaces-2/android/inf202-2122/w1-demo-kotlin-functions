fun main(args: Array<String>) {

    // a list can be null or its values can be null
    var voor :List<String> = listOf("jan","piet","joris","korneel")
    var nullbaar :MutableList<String?>? = mutableListOf("jan","piet",null,"korneel")
    nullbaar?.add("ashraf")

    // if returns an expression
    val temperature=20
    val isHot = temperature > 40
    val hoeHeet = if (temperature > 40) "heel heet" else "lauw"
//     val hoeHeet = temperature > 40? "heel heet" :  "lauw"




}